import { urlBase } from './config'

/**@returns The authentication token */
export async function authenticate() {
  const url = `${urlBase}:5000/v3/auth/tokens`
  const body = {
    "auth": {
      "identity": {
        "methods": [
          "password"
        ],
        "password": {
          "user": {
            "name": "wsfronteval",
            "domain": {
              "name": "Default"
            },
            "password": "wsfronteval"
          }
        }
      },
      "scope": {
        "project": {
          "id": "c1a432ae0bbc4dd690853b574129f25a"
        }
      }
    }
  }

  try {
    let response = await fetch(
      url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
      }
    );
    const token = response.headers.get('X-Subject-Token')
    let responseJson = await response.json();
    console.log(responseJson)
    console.log(response)
    return token;
  } catch (error) {
    console.error(error);
  }
}