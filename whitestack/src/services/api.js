import { urlBase } from './config'
import { fetchGet } from './helpers'

export async function getImages(token) {
  const res = await fetchGet(`${urlBase}:9292/v2/images`, token);
  if (res !== null) {
    return res.images
  }
  return []
}

export async function getFlavors(token) {
  const res = await fetchGet(`${urlBase}:8774/v2.1/flavors`, token);
  if (res !== null) {
    return res.flavors
  }
  return []
}

export async function getNetworks(token) {
  const res = await fetchGet(`${urlBase}:9696/v2.0/networks`, token);
  if (res !== null) {
    return res.networks
  }
  return []
}