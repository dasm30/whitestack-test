import { urlBase, projectId } from './config'
import { fetchGet, fetchPost, fetchPut, fetchDelete } from './helpers'

export default class Servers {

  static async getAll(token) {
    const res = await fetchGet(`${urlBase}:8774/v2.1/servers/detail`, token);
    if (res !== null) {
      return res.servers
    }
    return []
  }
  
  static async create(token, server) {
    const res = await fetchPost(`${urlBase}:8774/v2.1/servers`, token, server);
    if (res !== null) {
      return res
    }
    return res
  }
  
  static async update(token, serverId, server) {
    const res = await fetchPut(`${urlBase}:8774/v2.1/${projectId}/servers/${serverId}`, token, server);
    if (res !== null) {
      return res
    }
    return res
  }
  
  static async remove(token, serverId) {
    const res = await fetchDelete(`${urlBase}:8774/v2.1/${projectId}/servers/${serverId}`, token);
    if (res !== null) {
      return res
    }
    return res
  }
}