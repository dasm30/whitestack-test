export async function fetchGet(url, token) {
  try {
    let response = await fetch(
      url,
      {
        method: 'GET',
        headers: {
          'X-Auth-Token': token,
        }
      }
    );
    return await response.json();
  } catch (error) {
    console.error(error);
    return null
  }
}

export async function fetchPost(url, token, body) {
  try {
    let response = await fetch(
      url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Auth-Token': token,
          'X-OpenStack-Nova-API-Version': '2.37'
        },
        body: JSON.stringify(body)
      }
    );
    return await response.json();
  } catch (error) {
    console.error(error);
    return null
  }
}

export async function fetchPut(url, token, body) {
  try {
    let response = await fetch(
      url,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'X-Auth-Token': token
        },
        body: JSON.stringify(body)
      }
    );
    return await response.json();
  } catch (error) {
    console.error(error);
    return null
  }
}

export async function fetchDelete(url, token) {
  try {
    let response = await fetch(
      url,
      {
        method: 'DELETE',
        headers: {
          'X-Auth-Token': token
        }
      }
    );
    return response;
    //return await response.json(); this is returning Unexpected end of JSON input (server side issue)
  } catch (error) {
    console.error(error);
    return null
  }
}