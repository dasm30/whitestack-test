import React, { Component } from 'react';
import { Grid, Row, Col, Panel, PanelGroup } from 'react-bootstrap';
import VMForm from './VMForm';
import Table from './Table';
import { authenticate } from '../services/auth';
import Servers from '../services/servers';
import { getImages, getFlavors, getNetworks } from '../services/api';
import { tokenKey } from './vars';
import _ from 'lodash';
import './styles.css';

const columns = [{
  dataField: 'name',
  text: 'Máquina',
  sort: true
}, {
  dataField: 'flavor',
  text: 'Flavor',
  sort: true,
  editable: false
}, {
  dataField: 'img',
  text: 'Imagen',
  sort: true,
  editable: false
}, {
  dataField: 'status',
  text: 'Estado',
  sort: true,
  editable: false
}]

export default class Main extends Component {

  constructor(props) {
    super(props)
    this.state = {
      servers: [],
      flavors: [],
      images: [],
      networks: [],
      loading: true,
      selected: [],
      /**Array of changed servers that need to be saved*/
      changed: []
    }

    this.handleSave = this.handleSave.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.refreshServers = this.refreshServers.bind(this)
    this.afterSaveCell = this.afterSaveCell.bind(this)
  }

  async componentDidMount() {
    this.changeTableStyles()

    const { getAll } = Servers

    const token = await authenticate()
    localStorage.setItem(tokenKey, token)

    const servers = await getAll(token)
    console.log(servers)

    const flavors = await getFlavors(token)
    const images = await getImages(token)
    const networks = await getNetworks(token)

    this.setState({
      servers,
      flavors,
      images,
      networks,
      loading: false
    })
  }

  changeTableStyles() {
    const pageDropDown = document.getElementById('pageDropDown')
    pageDropDown.className = pageDropDown.className.replace('btn-default btn-secondary', 'btn-info')
  }

  getServers() {
    const { flavors, images, servers } = this.state
    return _.map(servers, (server) => {
      console.log('servers:')
      console.log(server)
      return {
        id: server.id,
        name: server.name,
        flavor: _.find(flavors, (x) => x.id === server.flavor.id).name,
        img: _.find(images, (x) => x.id === server.image.id).name,
        status: server.status
      }
    })
  }

  handleOnSelect = (row, isSelect) => {
    if (isSelect) {
      this.setState(() => ({
        selected: [
          ...this.state.selected,
          {
            id: row.id,
            name: row.name
          }
        ]
      }));
    } else {
      this.setState(() => ({
        selected: this.state.selected.filter(x => x.id !== row.id)
      }));
    }
  }

  handleOnSelectAll = (isSelect, rows) => {
    const servers = rows.map(r => {
      return {
        id: r.id,
        name: r.name
      }
    });
    if (isSelect) {
      this.setState(() => ({
        selected: servers
      }));
    } else {
      this.setState(() => ({
        selected: []
      }));
    }
  }

  afterSaveCell(newValue, row, column) {
    let { servers } = this.state
    let index = _.findIndex(servers, x => x.id === row.id)
    switch (column.dataField) {
      case 'name': {
        servers[index].name = newValue
        break
      }
      default: {
        console.log('default case..')
      }
    }
    console.log(servers)
    this.setState({
      servers,
      changed: [
        ...this.state.changed,
        {
          id: row.id,
          name: newValue
        }
      ]
    })
  }

  async handleSave() {
    const { changed } = this.state
    const { update } = Servers

    let server = null,
      res = ''

    console.log('saving servers changes..')
    for (let i = 0; i < changed.length; i++) {
      server = changed[i]
      res = await update(localStorage.getItem(tokenKey), server.id, {
        server: {
          name: server.name
        }
      })
      console.log(res)
    }
    console.log('handle saving finished..')
    this.setState({ changed: [] })
    this.refreshServers()
  }

  async handleDelete() {
    let { selected, servers } = this.state
    const { remove } = Servers
    let server = null,
      res = ''

    console.log('saving servers changes..')
    const predicate = x => x.id === server.id
    for (let i = 0; i < selected.length; i++) {
      server = selected[i]
      res = await remove(localStorage.getItem(tokenKey), server.id)
      _.remove(servers, predicate)
      this.setState({ servers })
      console.log(res)
    }
    console.log('handle delete finished..')
  }

  async refreshServers() {
    const servers = await Servers.getAll(localStorage.getItem(tokenKey))
    this.setState({ servers })
  }

  render() {
    const { loading, flavors, images, networks } = this.state
    const noDataIndication = loading ? "Cargando..." : "Tabla vacía"

    return (
      <Grid>
        <Row className="show-grid">
          <Col xs={12} md={12}>
            <header className="m-top-3 ta-center">
              <h1>Gestión de Máquinas Virtuales</h1>
            </header>
          </Col>
        </Row>
        <Row className="show-grid m-3">
          <Col xs={12} md={12}>
            <VMForm
              loading={loading}
              flavors={flavors}
              images={images}
              networks={networks}
              onServerAdded={this.refreshServers} />
          </Col>
        </Row>
        <Row className="show-grid m-3">
          <Col xs={12} md={12}>
            <PanelGroup accordion id="vmFormPanels" defaultActiveKey="1">
              <Panel eventKey="2" bsStyle='primary'>
                <Panel.Heading>
                  <Panel.Title>Listado de Máquinas Virtuales</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                  <p>Haga click en el nombre de la máquina si desea editarlo</p>
                  <Table
                    columns={columns}
                    data={this.getServers()}
                    selected={this.state.selected}
                    noDataIndication={noDataIndication}
                    handleOnSelect={this.handleOnSelect}
                    handleOnSelectAll={this.handleOnSelectAll}
                    handleSave={this.handleSave}
                    handleDelete={this.handleDelete}
                    afterSaveCell={this.afterSaveCell}
                    disableSaveBt={this.state.changed.length === 0} />
                </Panel.Body>
              </Panel>
            </PanelGroup>
          </Col>
        </Row>
      </Grid>
    );
  }
}
