import React, { Component } from 'react';
import { Button, Glyphicon, Panel, PanelGroup } from 'react-bootstrap';
import FieldGroup from './FieldGroup';
import DropdownField from './DropdownField';
import Servers from '../services/servers';
import timeout from '../utils/timeout';
import { tokenKey } from './vars';
import _ from 'lodash';

export default class VMForm extends Component {

  constructor(props) {
    super(props);

    this.handleServerNameChange = this.handleServerNameChange.bind(this);
    this.handleFlavorSelect = this.handleFlavorSelect.bind(this);
    this.handleImageSelect = this.handleImageSelect.bind(this);
    this.handleNetworkSelect = this.handleNetworkSelect.bind(this);
    this.onAddVM = this.onAddVM.bind(this);

    this.state = {
      serverName: '',
      image: 'Cargando...',
      flavor: 'Cargando...',
      network: 'Cargando...',
      imageId: '',
      flavorId: '',
      networkId: ''
    };
  }

  async componentDidMount() {
    while (this.props.loading) {
      await timeout(500)
    }

    this.setState({
      flavor: this.props.flavors[0].name,
      image: this.props.images[0].name,
      network: this.props.networks[0].name
    })
  }

  getNames(array) {
    return _.map(array, (value, index) => {
      return value.name
    })
  }

  getValidationState() {
    const length = this.state.serverName.length;
    if (length > 2) return 'success';
    else if (length > 1) return 'warning';
    else if (length > 0) return 'error';
    return null;
  }

  handleServerNameChange(e) {
    this.setState({ serverName: e.target.value });
  }

  handleFlavorSelect(value) {
    const { flavors } = this.props
    this.setState({ flavor: value, flavorId: _.find(flavors, (x) => x.name === value).id });
  }
  handleImageSelect(value) {
    const { images } = this.props
    this.setState({ image: value, imageId: _.find(images, (x) => x.name === value).id });
  }
  handleNetworkSelect(value) {
    const { networks } = this.props
    this.setState({ network: value, networkId: _.find(networks, (x) => x.name === value).id });
  }

  async onAddVM() {
    const { serverName, flavorId, imageId, networkId } = this.state
    const res = await Servers.create(localStorage.getItem(tokenKey), {
      "server": {
        "name": serverName,
        "imageRef": imageId,
        "flavorRef": flavorId,
        "max_count": 1,
        "min_count": 1,
        "networks": [{
          "uuid": networkId
        }]
      }
    })
    console.log(res)
    console.log('Server added!')
    if (this.props.onServerAdded) {
      this.props.onServerAdded()
    }
  }

  render() {
    return (
      <PanelGroup accordion id="vmFormPanels" defaultActiveKey="1">
        <Panel eventKey="1" bsStyle='primary'>
          <Panel.Heading>
            <Panel.Title toggle>Nueva Máquina Virtual</Panel.Title>
          </Panel.Heading>
          <Panel.Body collapsible>
            <form>
              <FieldGroup
                id="vmName"
                type="text"
                label="Nombre"
                placeholder="Mi Máquina Virtual"
                validation={this.getValidationState()}
                onChange={this.handleServerNameChange}
              />
              <DropdownField
                id='flavor-dropdown'
                label='Flavor'
                options={this.getNames(this.props.flavors)}
                onSelect={this.handleFlavorSelect}
                value={this.state.flavor} />
              <DropdownField
                id='image-dropdown'
                label='Imagen'
                options={this.getNames(this.props.images)}
                onSelect={this.handleImageSelect}
                value={this.state.image} />
              <DropdownField
                id='network-dropdown'
                label='Red'
                options={this.getNames(this.props.networks)}
                onSelect={this.handleNetworkSelect}
                value={this.state.network} />
              <Button
                bsStyle="primary"
                className='float-r'
                disabled={this.getValidationState() !== 'success' || this.props.loading}
                onClick={this.onAddVM}>
                <Glyphicon glyph="plus" /> Agregar
              </Button>
            </form>
          </Panel.Body>
        </Panel>
      </PanelGroup>
    );
  }
}