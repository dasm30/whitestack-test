import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import paginationFactory from 'react-bootstrap-table2-paginator';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ConfirmModal from './Modals/ConfirmModal';

export default class Table extends Component {

  static propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    selected: PropTypes.arrayOf(PropTypes.object).isRequired
  }

  static defaultProps = {
    columns: [{
      dataField: '1',
      text: 'Column 1'
    }, {
      dataField: '2',
      text: 'Column 2'
    }, {
      dataField: '3',
      text: 'Column 3'
    }],
    data: [],
    selelected: []
  }

  constructor(props) {
    super(props)
    this.state = {
      showConfirmModal: false
    }
    this.toggleConfirmModal = this.toggleConfirmModal.bind(this)
    this.onConfirm = this.onConfirm.bind(this)
  }

  toggleConfirmModal() {
    this.setState({ showConfirmModal: !this.state.showConfirmModal })
  }

  onConfirm() {
    if(this.props.handleDelete) {
      this.props.handleDelete()
    }
    this.toggleConfirmModal()
  }

  render() {
    const selectRow = {
      mode: 'checkbox',
      selected: _.map(this.props.selected, (s) => s.id),
      onSelect: this.props.handleOnSelect,
      onSelectAll: this.props.handleOnSelectAll
    };

    return (
      <div>
        <BootstrapTable
          keyField='id'
          data={this.props.data}
          columns={this.props.columns}
          cellEdit={cellEditFactory({
            mode: 'click',
            beforeSaveCell: (oldValue, newValue, row, column) => { console.log('Before Saving Cell!!'); },
            afterSaveCell: (oldValue, newValue, row, column) => {
              console.log('After Saving Cell!!');
              if (this.props.afterSaveCell && oldValue !== newValue) {
                this.props.afterSaveCell(newValue, row, column)
              }
            }
          })}
          pagination={paginationFactory()}
          noDataIndication={this.props.noDataIndication}
          selectRow={selectRow} />
        <Button
          bsStyle="primary"
          className='float-r'
          disabled={this.props.disableSaveBt}
          onClick={this.props.handleSave}>
          <Glyphicon glyph="floppy-disk" /> Guardar
        </Button>
        <Button
          bsStyle={null}
          className='float-r'
          disabled={this.props.selected.length === 0}
          onClick={this.toggleConfirmModal}>
          <Glyphicon glyph="trash" /> Eliminar
        </Button>
        <ConfirmModal
          show={this.state.showConfirmModal}
          header='Eliminar Máquina(s) Virtual(es)'
          body='¿Está seguro que desea eliminar las máquinas virtuales seleccionadas?'
          onConfirm={this.onConfirm}
          onCancel={this.toggleConfirmModal}
          onClose={this.toggleConfirmModal} />
      </div>
    )
  }
}