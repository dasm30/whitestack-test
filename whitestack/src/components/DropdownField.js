import React, { Component } from 'react';
import { SplitButton, MenuItem, FormGroup, ControlLabel } from 'react-bootstrap';
import PropTypes from 'prop-types'
import './styles.css'

export default class DropdownField extends Component {

  static propTypes = {
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    onSelect: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
  }

  getOptions() {
    return this.props.options.map((opt, index) => {
      return <MenuItem key={index} eventKey={opt} active={this.props.value === opt} onSelect={this.props.onSelect}>{opt}</MenuItem>
    })
  }

  render() {
    return (
      <FormGroup controlId={this.props.id}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <div className='btn-flex'>
          <SplitButton
            bsStyle={'info'}
            title={this.props.value}
            id={this.props.id}
            disabled={this.props.value === 'Cargando...'}
          >
            {this.getOptions()}
          </SplitButton>
        </div>
      </FormGroup>
    )
  }
}