import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'

export default class ConfirmModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      show: false
    }
    this.onConfirm = this.onConfirm.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.show !== null) {
      this.setState({ show: nextProps.show })
    }
  }

  onConfirm() {
    if(this.props.onConfirm) {
      this.props.onConfirm()
    }
    this.setState({show: false})
  }

  render() {

    return (
      <div>
        <Modal show={this.state.show} onHide={this.props.onClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.header}</Modal.Title>
          </Modal.Header>

          <Modal.Body>{this.props.body}</Modal.Body>

          <Modal.Footer>
            <Button onClick={this.props.onCancel}>{this.props.cancelBt || 'NO'}</Button>
            <Button bsStyle="primary" onClick={this.onConfirm}>
              {this.props.confirmBt || 'SI'}
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}